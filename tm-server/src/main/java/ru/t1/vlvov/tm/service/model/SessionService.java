package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.model.ISessionRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.ISessionService;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
