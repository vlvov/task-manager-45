package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.dto.*;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.api.service.model.IUserService;

public interface IServiceLocator {

    @Nullable
    ILoggerService getLoggerService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectService getProjectGraphService();

    @NotNull
    IUserService getUserGraphService();

    @Nullable
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserServiceDTO getUserService();

    @Nullable
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionServiceDTO getSessionService();

}