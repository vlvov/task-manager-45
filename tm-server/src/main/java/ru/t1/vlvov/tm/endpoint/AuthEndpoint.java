package ru.t1.vlvov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.dto.request.UserLoginRequest;
import ru.t1.vlvov.tm.dto.request.UserLogoutRequest;
import ru.t1.vlvov.tm.dto.response.UserLoginResponse;
import ru.t1.vlvov.tm.dto.response.UserLogoutResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @NotNull IAuthService authService = serviceLocator.getAuthService();
        @NotNull String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull IAuthService authService = serviceLocator.getAuthService();
        authService.logout(request.getToken());
        return new UserLogoutResponse();
    }

}
