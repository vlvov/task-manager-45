package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    String getDatabaseHbm2ddl();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseSecondLvlCash();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseUseQueryCash();

    @NotNull
    String getDatabaseUseMinPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

    @NotNull
    String getDatabaseFormatSql();

}
