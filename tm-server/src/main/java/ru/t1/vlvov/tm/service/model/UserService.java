package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.model.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.*;
import ru.t1.vlvov.tm.exception.user.ExistsLoginException;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.model.UserRepository;
import ru.t1.vlvov.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    protected IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setEmail(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setRole(role);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByLogin(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        remove(model);
        return model;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        update(model);
        return model;
    }

    @Override
    @NotNull
    public User updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setFirstName(firstName);
        model.setLastName(lastName);
        model.setMiddleName(middleName);
        update(model);
        return model;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @NotNull
    public User lockUserByLogin(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(true);
        update(model);
        return model;
    }

    @Override
    @NotNull
    public User unlockUserByLogin(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(false);
        update(model);
        return model;
    }

}
