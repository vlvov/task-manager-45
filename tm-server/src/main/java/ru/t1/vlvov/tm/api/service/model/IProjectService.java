package ru.t1.vlvov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @NotNull String description
    );

}
