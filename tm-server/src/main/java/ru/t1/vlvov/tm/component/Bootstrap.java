package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.*;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.api.service.dto.*;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.endpoint.*;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.service.*;
import ru.t1.vlvov.tm.service.dto.*;
import ru.t1.vlvov.tm.service.model.ProjectService;
import ru.t1.vlvov.tm.service.model.UserService;
import ru.t1.vlvov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.FileWriter;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private final IProjectService projectGraphService = new ProjectService(connectionService);

    @NotNull
    private final ISessionServiceDTO sessionService = new SessionServiceDTO(connectionService);

    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(connectionService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @NotNull
    private final IUserService userGraphService = new UserService(connectionService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        connectionService.factory();

        registry(domainEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(taskEndpoint);
        registry(authEndpoint);
    }


    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void initDemoData() {
        final UserDTO user1 = userService.create("test", "test", Role.USUAL);
        final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(admin.getId(), "First", "First Project").setStatus(Status.NOT_STARTED);
        projectService.create(admin.getId(), "Second", "Second Project").setStatus(Status.IN_PROGRESS);
        projectService.create(user1.getId(), "Third", "Third Project").setStatus(Status.NOT_STARTED);
        projectService.create(user1.getId(), "Force", "Force be with you").setStatus(Status.COMPLETED);

        taskService.create(admin.getId(), "Third", "Third Task");
        taskService.create(admin.getId(), "Second", "Second Task");
        taskService.create(user1.getId(), "First", "First Task");
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
    }

    public void run() {
        initPID();
        //initDemoData();
        loggerService.info("**TASK-MANAGER SERVER STARTED**");
        //backup.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
