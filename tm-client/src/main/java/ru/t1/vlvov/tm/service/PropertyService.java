package ru.t1.vlvov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    final String FILE_NAME = "application.properties";

    @NotNull
    final String SERVER_PORT_KEY = "server.port";

    @NotNull
    final String SERVER_HOST_KEY = "server.host";

    @NotNull
    final String EMPTY_VALUE = "---";

    @NotNull
    final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, EMPTY_VALUE);
    }

}
